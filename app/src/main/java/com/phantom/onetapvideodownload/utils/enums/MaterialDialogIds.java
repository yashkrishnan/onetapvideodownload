package com.phantom.onetapvideodownload.utils.enums;

public enum MaterialDialogIds {
    DefaultDownloadLocation(0), VideoDownloadLocation(1);

    private final int mValue;
    MaterialDialogIds(int value) {
        mValue = value;
    }
}
