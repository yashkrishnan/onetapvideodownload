package com.phantom.onetapvideodownload.utils;

public interface OnDownloadChangeListener {
    void onDownloadAdded();
    void onDownloadRemoved();
    void onDownloadInfoUpdated();
}
